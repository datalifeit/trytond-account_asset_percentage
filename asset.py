# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pyson import Eval
from decimal import Decimal
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Asset(metaclass=PoolMeta):
    __name__ = 'account.asset'

    depreciation_period = fields.Many2One('account.asset.period',
        'Depreciation period',
        states={
            'invisible': Eval('depreciation_method') != 'percentage',
            'required': Eval('depreciation_method') == 'percentage'
        })

    @classmethod
    def __setup__(cls):
        super(Asset, cls).__setup__()
        cls.depreciation_method.selection.append(('percentage', 'Percentage'))

    def _find_percentage(self, index):
        for line in self.depreciation_period.lines:
            if line.period == index:
                return line.percentage
        return self.depreciation_period.default_percentage

    def compute_depreciation(self, amount, date, dates):
        if self.depreciation_method == 'percentage':
            amount = (self.value - self.get_depreciated_amount()
                - self.residual_value)
            percentage = self._find_percentage(dates.index(date) + 1)
            depreciation = (amount * Decimal(percentage) / 100) or 0.0
            return self.company.currency.round(depreciation)

        return super().compute_depreciation(amount, date, dates)


class AssetPeriod(DeactivableMixin, ModelSQL, ModelView):
    '''Account Asset Period'''
    __name__ = 'account.asset.period'

    name = fields.Char('Name', size=None, required=True, translate=True)
    lines = fields.One2Many('account.asset.period.line',
        'asset_period', 'Lines')
    default_percentage = fields.Function(fields.Float(
        'Default depreciation percentage', digits=(3, 3)),
        'get_default_percentage')

    def get_default_percentage(self, name=None):
        for line in self.lines:
            if not line.period:
                return line.percentage

    @classmethod
    def validate(cls, periods):
        cls.check_lines(periods)

    @classmethod
    def check_lines(cls, periods):
        for period in periods:
            lines = [line for line in period.lines if not line.period]
            if len(lines) > 1:
                raise UserError(gettext(
                    'account_asset_percentage.msg_exceed_one_line',
                    len_lines=len(lines)))
            elif len(lines) == 0:
                total = sum(line.percentage
                     for line in period.lines if line.percentage)
                if total != 100:
                    raise UserError(gettext(
                        'account_asset_percentage.msg_total_not_hundred'))


class AssetPeriodLine(ModelSQL, ModelView):
    '''Account Asset Period Line'''
    __name__ = 'account.asset.period.line'

    asset_period = fields.Many2One('account.asset.period', 'Asset period',
        required=True, ondelete='CASCADE')
    period = fields.Integer('Period',
        domain=['OR',
            ('period', '>=', 1),
            ('period', '=', None)],
        help=('The number of the asset line. Let it blank for default value.'))
    percentage = fields.Float('Depreciation percentage',
        domain=[
            ('percentage', '>=', 0),
            ('percentage', '<=', 100)],
        digits=(3, 3))

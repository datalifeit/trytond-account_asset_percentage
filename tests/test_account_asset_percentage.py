# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountAssetPercentageTestCase(ModuleTestCase):
    """Test Account Asset Percentage module"""
    module = 'account_asset_percentage'


del ModuleTestCase

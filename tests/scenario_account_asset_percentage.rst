=================================
Account Asset Percentage Scenario
=================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.account_asset.tests.tools \
    ...     import add_asset_accounts
    >>> today = datetime.date.today()

Install account_asset_percentage::

    >>> config = activate_modules('account_asset_percentage')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = add_asset_accounts(get_accounts(company), company)
    >>> revenue = accounts['revenue']
    >>> asset_account = accounts['asset']
    >>> expense = accounts['expense']
    >>> depreciation_account = accounts['depreciation']

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.account_asset = asset_account
    >>> account_category.account_depreciation = depreciation_account
    >>> account_category.save()


Create an asset::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> asset_product = Product()
    >>> asset_template = ProductTemplate()
    >>> asset_template.name = 'Asset'
    >>> asset_template.type = 'assets'
    >>> asset_template.default_uom = unit
    >>> asset_template.list_price = Decimal('10000')
    >>> asset_template.depreciable = True
    >>> asset_template.account_category = account_category
    >>> asset_template.depreciation_duration = 4
    >>> asset_template.save()
    >>> asset_product, = asset_template.products

Create supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Buy an asset::

    >>> Invoice = Model.get('account.invoice')
    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> supplier_invoice = Invoice(type='in')
    >>> supplier_invoice.party = supplier
    >>> invoice_line = InvoiceLine()
    >>> supplier_invoice.lines.append(invoice_line)
    >>> invoice_line.product = asset_product
    >>> invoice_line.quantity = 1
    >>> invoice_line.unit_price = Decimal('10000')
    >>> invoice_line.account == asset_account
    True
    >>> supplier_invoice.invoice_date = today + relativedelta(day=1, month=1)
    >>> supplier_invoice.click('post')
    >>> supplier_invoice.state
    'posted'
    >>> invoice_line, = supplier_invoice.lines
    >>> (asset_account.debit, asset_account.credit) == \
    ...     (Decimal('10000'), Decimal('0'))
    True

Create periods depreciation::

    >>> Period = Model.get('account.asset.period')
    >>> period = Period(name="period1")
    >>> line = period.lines.new()
    >>> line.percentage = 6.25
    >>> line.period = 1

    >>> period.save()
    Traceback (most recent call last):
      ...
    trytond.exceptions.UserError: Must define either a line with no period or total percentage must be "100". - 

    >>> line2 = period.lines.new()
    >>> line2.percentage = 12.5
    >>> period.save()

Depreciate the asset::

    >>> Asset = Model.get('account.asset')
    >>> asset = Asset()
    >>> asset.depreciation_period = period
    >>> asset.depreciation_method = 'percentage'
    >>> asset.product = asset_product
    >>> asset.supplier_invoice_line = invoice_line
    >>> asset.frequency = 'yearly'
    >>> asset.value
    Decimal('10000.00')
    >>> asset.start_date = datetime.date(2018, 1, 1)
    >>> asset.end_date = datetime.date(2026, 1, 1)
    >>> asset.residual_value = Decimal('0')
    >>> asset.click('create_lines')
    >>> len(asset.lines)
    9
    >>> [l.depreciation for l in asset.lines]
    [Decimal('625.00'), Decimal('1250.00'), Decimal('1250.00'), Decimal('1250.00'), Decimal('1250.00'), Decimal('1250.00'), Decimal('1250.00'), Decimal('1250.00'), Decimal('625.00')]
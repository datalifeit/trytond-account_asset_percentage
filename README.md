datalife_account_asset_percentage
=================================

The account_asset_percentage module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_asset_percentage/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_asset_percentage)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
